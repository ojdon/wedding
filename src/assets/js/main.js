window.addEventListener("load", function() {

    const showGuestDiv = document.getElementById('showGuest');
    document.getElementById("year").innerHTML = "" + new Date().getFullYear();

    window.showGuest = function showGuestInput() {
        window.showGuestDiv.style.display = 'block';
    }

    window.hideGuest = function hideGuestInput() {
        window.showGuestDiv.style.display = 'none';
    }

});

